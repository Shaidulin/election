import React from 'react';
import ReactDOM from 'react-dom';
import Application from './components/Application.jsx';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers/reducer.jsx';

const store = createStore(reducer);

ReactDOM.render(
	<Provider store={store}>
		<Application/>
	</Provider>,
	document.getElementById('mount-point')
);
