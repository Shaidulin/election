
const initialState = {
  count1:0,
  count2:0,
};

export default function reducer(state = initialState, action){
  switch (action.type) {
     case 'INCREMENT_COUNT1':
        return Object.assign({}, state, {
          count1: state.count1+1
          });

     case 'INCREMENT_COUNT2':
      return Object.assign({}, state, {
            count2: state.count2+1
            });

    default:
        return state;
  }
} 