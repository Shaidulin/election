import React,{Component} from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import { connect } from 'react-redux';

class GraphBar extends Component {

  render() {
  	const data = [
	   {name: 'Vote', Obama: this.props.obamaCount, Trump: this.props.trumpCount},
	   ];
    return (
      <div>
    		<BarChart width={600} height={300} data={data} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
    		       <XAxis dataKey="name"/>
    		       <YAxis/>
    		       <CartesianGrid strokeDasharray="3 3"/>
    		       <Tooltip/>
    		       <Bar dataKey="Obama" fill="#3064b0" />
    		       <Bar dataKey="Trump" fill="#bc0000" />
    		</BarChart>
      </div>
    );
  }
}

export default connect(
  state => ({
    obamaCount:state.count1,
    trumpCount:state.count2
  }),
)(GraphBar);


