import React,{ Component } from 'react';
import  { ResponsiveContainer,Legend, PieChart, Pie, Sector, Cell }  from 'recharts';
import { connect } from 'react-redux';

class GraphPie extends Component {

  render() {
    let obamaCount = this.props.obamaCount;
    let trumpCount = this.props.trumpCount;

    if (this.props.obamaCount===0&&this.props.trumpCount===0) {
      obamaCount=1;
      trumpCount=1;
    }

  const data = [{name: 'Оbama', value:obamaCount }, {name: 'Trump', value: trumpCount}];
  const colors = ['#3064b0', '#bc0000'];
  const radian = Math.PI / 180;                    
  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x  = cx + radius * Math.cos(-midAngle * radian);
    const y = cy  + radius * Math.sin(-midAngle * radian);
    return (
      <text key={index} x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'}  dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

   return (
    <div>
      <PieChart width={800} height={400} onMouseEnter={this.onPieEnter}>
        <Pie
          data={data} 
          cx={300} 
          cy={200} 
          labelLine={false}
          label={renderCustomizedLabel}
          outerRadius={80} 
          fill="#8884d8"
        >
          {
            data.map((entry, index) => <Cell key={index} fill={colors[index % colors.length]}/>)
          }
        </Pie>
    </PieChart>
    </div>
  );
}
}


export default connect(
  state => ({
    obamaCount:state.count1,
    trumpCount:state.count2,
  })
)(GraphPie);


