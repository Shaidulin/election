import React,{ Component } from 'react';
import Vote from './Vote.jsx'
import CarouselVote from './Carousel.jsx';

class Application extends Component {

  render() {
    return (
      <div>
        <CarouselVote />
        <Vote />
      </div>
    );
  }
}

export default Application;
