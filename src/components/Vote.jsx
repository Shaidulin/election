import React,{Component} from 'react';
import { connect } from 'react-redux';

class Vote extends Component {

  obamaVote() {
    this.props.obamaVote();
  }

  trumpVote() {
    this.props.trumpVote ();
  }

  render() {
    return (
      <div>
        <button className="button__obama" onClick={this.obamaVote.bind(this)} >Obama</button>
        <button className="button__trump" onClick={this.trumpVote.bind(this)} >Trump</button>
      </div>
    );
  }
}

export default connect(
  state => ({
    obamaCount:state.count1,
    trumpCount:state.count2
  }),

  dispatch => ({
    obamaVote:()=> {
      dispatch({
        type: 'INCREMENT_COUNT1'
      });
    }, 

    trumpVote:()=> {
      dispatch({
        type: 'INCREMENT_COUNT2'
      });
    }
  })
)(Vote);