import React,{ Component } from 'react';
import { connect } from 'react-redux';
import Carousel  from 'nuka-carousel';
import GraphBar from './GraphBar.jsx'
import GraphPie from './GraphPie.jsx'
import '../styles/style.less';

export default  class CarouselVote extends React.Component {
  render() {
    return (
      <div className="carousel-vote">
        <Carousel>
          <GraphBar />
          <GraphPie />
        </Carousel>
      </div>
    );
  }
}
