Реализован компонент для голосования (Трамп/Обама).
На странице:
1) 2 кнопки для голосования.
2) Карусель из 2 графиков recharts http://recharts.org/#/en-US/.

В карусели 2 графика: linechart, piechart.При клике за каждого кандидата обновляется Store, и соответственно - графики.  Linechart имеет 2 линии, показывающие колличество голосов а каждого кандидата. Piechart, соответственно, показывает пропорции.

Election
=============

# Installation

nodejs `v6.9.0` and upper.

```bash
git clone git@gitlab.com:Shaidulin/election.git
cd election
npm install
```
# Launch
```
npm run devserver
localhost:8090
```